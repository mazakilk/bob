ifeq ($(PREFIX),)
	PREFIX:=/usr/local
endif

.PHONY: all
all: bin/dfi bin/dfi_ical_peek

bin/dfi: src/dfi.sh
	mkdir -p bin
	cp src/dfi.sh bin/dfi
	chmod a+x bin/dfi

bin/dfi_ical_peek: src/ical_peek/ical_peek.c
	gcc -o bin/dfi_ical_peek src/ical_peek/ical_peek.c

.PHONY: clean
clean:
	rm -rf bin

.PHONY: install
install: all
	install -d $(DESTDIR)$(PREFIX)/bin
	install bin/dfi $(DESTDIR)$(PREFIX)/bin
	install bin/dfi_ical_peek $(DESTDIR)$(PREFIX)/bin

.PHONY: uninstall
uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/dfi
	rm -f $(DESTDIR)$(PREFIX)/bin/dfi_ical_peek
